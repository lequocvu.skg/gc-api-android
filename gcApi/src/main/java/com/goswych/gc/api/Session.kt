/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api

import com.goswych.gc.api.models.AccessTokenModel
import com.goswych.gc.api.models.CatalogModel
import com.goswych.gc.api.models.TransactionModel
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.JsonClass

object Session {

    /**
     * Store accessTokenResult, not just access token, since we do need to check for expiration
     */
    @JvmStatic
    var accessTokenResult: AccessTokenModel.Result? = null

    fun isValid(): Boolean {
        accessTokenResult ?: return false
        val str = accessTokenResult?.access_token ?: return false

        if (str.isBlank()) return false

        // TODO: check for expiration
        val expireOn = accessTokenResult?.expires_on?.toLong() ?: return false
        val now = System.currentTimeMillis()
        if (expireOn >= now) {
            Util.log("- expired, invalid: expires_on=${accessTokenResult?.expires_on} vs now=$now")
        } else Util.log("- valid: expires_on=${accessTokenResult?.expires_on} vs now=$now")

        return (expireOn < now)
    }

    fun getAccessToken(): String {
        return accessTokenResult?.access_token!!

    }

    /**
     * cache catalog result for UI
     */
    @JvmStatic
    var catalogResult: CatalogModel.Result? = null

    /**
     * cache transaction
     */
    @JvmStatic
    var latestTransaction: TransactionModel.OrderResult? = null


    /**
     * Api keys are stored in json format
     */
    @JsonClass(generateAdapter = true)
    data class ApiKeys(
        val ocpApimSubscriptionKey: String,
        val client_id: String, val client_secret: String, val resource: String,
        val clientId: String, val accountId: String, val apiKey: String
    )

    lateinit var apiKeys: ApiKeys

    /**
     * Inject keys to lib
     * Must be called during very 1st app init
     */
    @JvmStatic
    fun setApiKeys(keys: String) {
        val adapter: JsonAdapter<ApiKeys> = Factory.moshi.adapter(ApiKeys::class.java)
        apiKeys = adapter.fromJson(keys)!!
    }
}