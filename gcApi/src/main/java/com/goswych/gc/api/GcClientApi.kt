/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api

import com.goswych.gc.api.Util.errorHandler
import com.goswych.gc.api.models.AccessTokenModel
import com.goswych.gc.api.models.CatalogModel
import com.goswych.gc.api.models.TransactionModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

typealias AccessTokenResultHandler = (AccessTokenModel.Result) -> Unit
typealias ErrorHandler = (Throwable) -> Unit
typealias CatalogResultHandler = (CatalogModel.Result) -> Unit
typealias OrderGCResultHandler = (TransactionModel.OrderResult) -> Unit
typealias OrderConfirmResultHandler = (TransactionModel.ConfirmResult) -> Unit

/**
 * Async client api with session & key management
 * Support Java interoperability
 */
object GcClientApi {

    /**
     * openSession
     */
    @JvmStatic
    fun openSession(
        onSuccess: AccessTokenResultHandler? = null,
        onError: ErrorHandler? = null
    ): Disposable? {
        if (Session.isValid()) {
            Util.log("- openSession: session already exist -> do nothing")
            return null
        }
        Util.log("- openSession")
        Session.accessTokenResult = null

        return GcKeyRestApi.requestAccessToken()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    Session.accessTokenResult = result
                    onSuccess?.invoke(result)
                },
                { error ->
                    errorHandler(error) ?: return@subscribe
                    onError?.invoke(error)
                }
            )
    }

    /**
     * getCatalog
     */
    @JvmStatic
    fun getCatalog(
        onSuccess: CatalogResultHandler? = null,
        onError: ErrorHandler? = null,
        pageNo: String? = null,
        pageSize: String? = null,
        countryCode: String? = null
    ): Disposable? {
        Util.log("- getCatalog")
        if (!Session.isValid()) {
            Util.log("- No session detected. Opening new session ...")
            return openSession(
                { getCatalog(onSuccess, onError, pageNo, pageSize, countryCode) },
                onError
            )
        }

        return GcKeyRestApi.getCatalog(pageNo, pageSize, countryCode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    Session.catalogResult = result
                    onSuccess?.invoke(result)
                },
                { error ->
                    // Unauthorized. Access token is invalid. Session expired. Request new one.
                    if ((error as? HttpException)?.code() == 401) {
                        Util.log("- Session expired. Opening new session ...")
                        openSession(
                            { getCatalog(onSuccess, onError, pageNo, pageSize, countryCode) },
                            onError
                        )
                        return@subscribe
                    }

                    errorHandler(error) ?: return@subscribe
                    onError?.invoke(error)
                }
            )
    }


    /**
     * orderGiftCard
     */
    @JvmStatic
    fun orderGiftCard(
        onSuccess: OrderGCResultHandler? = null,
        onError: ErrorHandler? = null,
        brandId: String,
        productId: String,
        amount: Int,
        recipientEmail: String,
        recipientPhoneNumber: String,
        senderPhone: String,
        accessCode: String? = null
    ): Disposable? {

        Util.log("- order GiftCard")
        if (Session.isValid()) {
            return GcKeyRestApi.orderGiftCard(brandId, productId, amount, recipientEmail, recipientPhoneNumber, senderPhone, accessCode)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    { result ->
                        Session.latestTransaction = result
                        onSuccess?.invoke(result)
                    },
                    { error ->
                        // Unauthorized. Access token is invalid. Session expired. Request new one.
                        if ((error as? HttpException)?.code() == 401) {
                            Util.log("- Session expired. Opening new session ...")
                            openSession(
                                {
                                    orderGiftCard(onSuccess, onError, brandId, productId, amount, recipientEmail, recipientPhoneNumber, senderPhone, accessCode)
                                },
                                onError
                            )
                            return@subscribe
                        }

                        errorHandler(error) ?: return@subscribe
                        onError?.invoke(error)
                    }
                )
        }

        Util.log("- No session detected. Opening new session ...")
        return openSession(
            { orderGiftCard(onSuccess, onError, brandId, productId, amount, recipientEmail, recipientPhoneNumber, senderPhone, accessCode) },
            onError
        )
    }

    /**
     * orderConfirm
     * Access code: use access code from orderGiftCard result for order confirm's parameter
     */
    @JvmStatic
    fun orderConfirm(
        onSuccess: OrderConfirmResultHandler? = null,
        onError: ErrorHandler? = null,
        orderId: String
    ): Disposable? {
        if (!Session.isValid()) {
            Util.log("- No session detected. Opening new session ...")
            return openSession(
                { orderConfirm(onSuccess, onError, orderId) },
                onError
            )
        }

        if (Session.latestTransaction?.AccessCode.isNullOrEmpty()) {
            Util.log("- Invalid AccessCode")
            return null
        }

        Util.log("- order Confirm")
        return GcKeyRestApi.orderConfirm(orderId, Session.latestTransaction!!.AccessCode)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe(
                { result ->
                    onSuccess?.invoke(result)
                },
                { error ->
                    // Unauthorized. Access token is invalid. Session expired. Request new one.
                    if ((error as? HttpException)?.code() == 401) {
                        Util.log("- Session expired. Opening new session ...")
                        openSession(
                            {
                                orderConfirm(onSuccess, onError, orderId)
                            },
                            onError
                        )
                        return@subscribe
                    }

                    errorHandler(error) ?: return@subscribe
                    onError?.invoke(error)
                }
            )
    }

}