/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api.models

object GiftCardModel {

    data class GiftCard(
        val giftCardId: String?,
        val giftCardNumber: String?,
        val imageUrl: String?,
        val giftPIN: String?,
        val retailer: String?,
        val barcodeValue: String?,
        val barcodeUrl: String?,
        val giftCardUrl: String?
    )
}