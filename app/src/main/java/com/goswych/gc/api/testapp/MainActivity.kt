/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api.testapp

import android.content.DialogInterface
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.goswych.gc.api.models.AccessTokenModel
import com.goswych.gc.api.GcClientApi
import com.goswych.gc.api.Session
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var disposable: Disposable? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        /**
         * Inject keys to the lib
         */
        Session.setApiKeys(BuildConfig.swych_api_keys)

        btn1.setOnClickListener {
            addText("openSession")
            disposable = GcClientApi.openSession(
                { addText("Access token received (${Session.accessTokenResult?.access_token?.count()})") },
                onError
            )
            if (disposable == null) addText("Session already opened")
        }

        btn2.setOnClickListener {
            addText("getCatalog")
            disposable = GcClientApi.getCatalog(
                { addText("Category: ${Session.catalogResult?.categoryList?.count()}\nCatalog: ${Session.catalogResult?.catalog?.count()}") },
                onError
            )
        }

        btn3.setOnClickListener {
            addText("Order")
            disposable = GcClientApi.orderGiftCard({
                addText("Ordered ${it.orderId}")
                showConfirmOrderDialog()
            }, onError, "6","6-1", 1, "sample@domain.com", "string", "string", "1234")
        }
    }

    private fun addText(text: String) {
        txt_result.text = "${txt_result.text}\n${text}"
    }

    private val onError: (Throwable) -> Unit = errorHandler@{ errorThrowable ->
        addText("${errorThrowable.message}")
        val accessTokenError = AccessTokenModel.parseError(errorThrowable) ?: return@errorHandler
        addText("${accessTokenError.error}\n${accessTokenError.error_description}")
    }

    private fun showConfirmOrderDialog() {
        val dialogBuilder = AlertDialog.Builder(this)

        dialogBuilder.setMessage("Do you want to confirm order this product ?")
            .setCancelable(false)
            .setPositiveButton("Confirm", DialogInterface.OnClickListener {
                    dialog, id ->
                disposable = GcClientApi.orderConfirm({
                    addText("Confirmed")}, onError, Session.latestTransaction!!.orderId , Session.latestTransaction!!.AccessCode
                    )
                dialog.dismiss()
            })
            .setNegativeButton("Cancel", DialogInterface.OnClickListener {
                    dialog, id -> dialog.cancel()
            })

        val alert = dialogBuilder.create()
        alert.setTitle("Confirm")
        alert.show()
    }

    override fun onPause() {
        super.onPause()
        disposable?.dispose()
    }
}
