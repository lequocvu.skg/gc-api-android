/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api.coroutine

import com.goswych.gc.api.models.AccessTokenModel
import com.goswych.gc.api.models.CatalogModel
import com.goswych.gc.api.Factory
import com.goswych.gc.api.models.TransactionModel
import retrofit2.Response
import retrofit2.http.*

/**
 * Low-level REST API
 * Using Kotlin Coroutine
 */
interface GcRestApi {

    /**
     * Kotlin Coroutine suspense func
     */
    @POST( "oauth2/token")
    @FormUrlEncoded
    suspend fun requestAccessToken(
        @Header("Ocp-Apim-Subscription-Key") ocpApimSubscriptionKey : String,
        @Field("grant_type") grant_type: String,
        @Field("client_id") client_id: String,
        @Field("client_secret") client_secret: String,
        @Field("resource") resource: String
    ) : Response<AccessTokenModel.Result>

    /**
     * Kotlin Coroutine suspense func
     */
    @GET( "api/v1/b2b/{accountId}/catalog")
    suspend fun getCatalog(
        @Header("Ocp-Apim-Subscription-Key") ocpApimSubscriptionKey : String,
        @Header("Authorization") Authorization : String,
        @Header("ApiKey") ApiKey : String,
        @Header("clientId") clientId : String,
        @Path("accountId") accountId: String,
        @Query("pageNo") pageNo: String? = null,
        @Query("pageSize") pageSize: String? = null,
        @Query("countryCode") countryCode: String? = null
    ) : Response<CatalogModel.Result>

    @PUT("api/v1/b2b/order")
    @Headers("Content-Type: application/json")
    fun orderGiftCard(
        @Header("Ocp-Apim-Subscription-Key") ocpApimSubscriptionKey: String,
        @Header("Authorization") Authorization: String,
        @Header("ApiKey") ApiKey: String,
        @Header("clientId") clientId: String,
        @Body content: TransactionModel.OrderGCRequest
    ): Response<TransactionModel.OrderResult>

    @POST("api/v1/b2b/order")
    @Headers("Content-Type: application/json")
    fun orderConfirm(
        @Header("Ocp-Apim-Subscription-Key") ocpApimSubscriptionKey: String,
        @Header("Authorization") Authorization: String,
        @Header("ApiKey") ApiKey: String,
        @Header("clientId") clientId: String,
        @Body content: TransactionModel.ConfirmOrderRequest
    ): Response<TransactionModel.ConfirmResult>

    companion object {
        val instance by lazy { Factory.create<GcRestApi>("https://api.swychover.io/sandbox/") }
    }

}