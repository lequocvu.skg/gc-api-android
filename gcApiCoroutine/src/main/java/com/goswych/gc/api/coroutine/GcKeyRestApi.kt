/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api.coroutine

import com.goswych.gc.api.models.AccessTokenModel
import com.goswych.gc.api.models.CatalogModel
import com.goswych.gc.api.Session
import retrofit2.Response

object GcKeyRestApi {

    suspend fun requestAccessToken(): Response<AccessTokenModel.Result> =
        GcRestApi.instance.requestAccessToken(
            Session.apiKeys.ocpApimSubscriptionKey,
            "client_credentials",
            Session.apiKeys.client_id,
            Session.apiKeys.client_secret,
            Session.apiKeys.resource
        )

    suspend fun getCatalog(
        pageNo: String? = null,
        pageSize: String? = null,
        countryCode: String? = null
    ): Response<CatalogModel.Result> =
        GcRestApi.instance.getCatalog(
            Session.apiKeys.ocpApimSubscriptionKey,
            Session.getAccessToken(),
            Session.apiKeys.apiKey,
            Session.apiKeys.clientId,
            Session.apiKeys.accountId,
            pageNo, pageSize, countryCode
        )

}