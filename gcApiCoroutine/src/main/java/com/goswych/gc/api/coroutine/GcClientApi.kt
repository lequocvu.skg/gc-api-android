/*
 * Copyright 2019 Swych Inc.
 * Author: Trung Vo
 */

package com.goswych.gc.api.coroutine

import android.util.Log
import com.goswych.gc.api.models.AccessTokenModel
import com.goswych.gc.api.models.CatalogModel
import com.goswych.gc.api.Session
import com.goswych.gc.api.Util
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.Response

/**
 * Java interop is not supported since Coroutine won't work with @JvmStatic
 */
object GcClientApi {
    val TAG = this.javaClass.name

    suspend fun openSession(): AccessTokenModel.Result? {
        if (Session.isValid()) {
            Log.d(TAG, "- openSession: session already exist -> do nothing")
            return null
        }
        Log.d(TAG, "- openSession")
        Session.accessTokenResult = null

        try {
            val response = GcKeyRestApi.requestAccessToken()
            // No need to await() that method because retrofit does that
            if (response.isSuccessful) {
                Session.accessTokenResult = response.body()
            } else {
                //Handle unsuccessful response
                Log.d(TAG, "- openSession: success=${response.isSuccessful}")
            }
        } catch (error: Throwable) {
            if (Util.errorHandler(error) != null) {
                throw error
            }
        }
        return Session.accessTokenResult
    }

    fun openSessionSync() {
        if (Session.isValid()) return
        Log.d(TAG, "- openSessionSync")

        // Background Thread
        val job = GlobalScope.launch(Dispatchers.IO) {
            openSession()
        }
    }


    suspend fun getCatalog(
        pageNo: String? = null,
        pageSize: String? = null,
        countryCode: String? = null
    ): CatalogModel.Result? {

        openSession()

        Log.d(TAG, "- getCatalog")
        try {
            val response = GcKeyRestApi.getCatalog(pageNo, pageSize, countryCode)
            // No need to await() that method because retrofit does that
            if (response.isSuccessful) {
                Session.catalogResult = response.body()
            } else {
                Log.d(TAG, "- getCatalog ${response.isSuccessful}")
            }
        } catch (error: Throwable) {
            if (Util.errorHandler(error) != null) {
                throw error
            }
        }

        return Session.catalogResult
    }
}